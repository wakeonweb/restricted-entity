<?php

error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('UTC');

if (!is_file($autoloadFile = __DIR__.'/../vendor/autoload.php')) {
    throw new \LogicException('Could not find autoload.php in vendor/. Did you run "composer install --dev"?');
}

$classLoader = require $autoloadFile;
$classLoader->add('Doctrine\\Tests\\', __DIR__ . '/../vendor/doctrine/orm/tests/');
$classLoader->add('UCS\\Component\\RestrictedEntity\\Tests\\', __DIR__);

unset($classLoader);

use Doctrine\Common\Annotations\AnnotationRegistry;

AnnotationRegistry::registerFile(__DIR__ . "/../Annotation/RestrictedEntity.php");
AnnotationRegistry::registerFile(__DIR__ . "/../Annotation/RestrictedEntityPath.php");
