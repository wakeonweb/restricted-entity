<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Tests\Models\Resolvers;

/* Imports */
use UCS\Component\RestrictedEntity\Resolver\ValueResolverInterface;

/**
 * Returns some value
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class NullValueResolver implements ValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'null_value';
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($userPath)
    {
        return null;
    }
}

