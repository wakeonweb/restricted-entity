<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Tests\Models\Partnership;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\Common\Collections\ArrayCollection;
use UCS\Component\RestrictedEntity\Annotation as UCS;

/**
 * Partnership employees class.
 *
 * @Entity
 * @Table(name="partnership_employees")
 *
 * @UCS\RestrictedEntity(strategy="first_not_null", paths={
 *  @UCS\RestrictedEntityPath(propertyPath="client.partner", userPath="partner"),
 *  @UCS\RestrictedEntityPath(propertyPath="client.group", userPath="group", resolver="some_value")
 * })
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class PartnershipEmployee
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", length=255, unique=true)
     */
    public $name;

    /**
     * @ManyToOne(targetEntity="PartnershipClient")
     * @JoinColumn(referencedColumnName="id")
     */
    public $client;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PartnershipEmployee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param PartnershipClient $client
     *
     * @return PartnershipUser
     */
    public function setClient(PartnershipClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return PartnershipClient
     */
    public function getClient()
    {
        return $this->client;
    }
}
