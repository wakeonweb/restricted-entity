<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Tests\Models\Partnership;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Partnership user class.
 *
 * @Entity
 * @Table(name="partnership_users")
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class PartnershipUser
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", length=255, unique=true)
     */
    public $username;

    /**
     * @ManyToOne(targetEntity="PartnershipPartner")
     * @JoinColumn(name="partner_id", referencedColumnName="id")
     */
    public $partner;

    /**
     * @ManyToOne(targetEntity="PartnershipGroup")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    public $group;

    /**
     * @ManyToOne(targetEntity="PartnershipClient")
     * @JoinColumn(name="client_id", referencedColumnName="id")
     */
    public $client;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return PartnershipUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param PartnershipPartner $partner
     *
     * @return PartnershipUser
     */
    public function setPartner(PartnershipPartner $partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return PartnershipPartner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param PartnershipGroup $group
     *
     * @return PartnershipUser
     */
    public function setGroup(PartnershipGroup $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return PartnershipGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param PartnershipClient $client
     *
     * @return PartnershipUser
     */
    public function setClient(PartnershipClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return PartnershipClient
     */
    public function getClient()
    {
        return $this->client;
    }
}
