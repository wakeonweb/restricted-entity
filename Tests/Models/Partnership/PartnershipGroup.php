<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Tests\Models\Partnership;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Partnership groups class.
 *
 * @Entity
 * @Table(name="partnership_groups")
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class PartnershipGroup
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", length=255, unique=true)
     */
    public $name;

    /**
     * @ManyToOne(targetEntity="PartnershipPartner")
     * @JoinColumn(name="partner_id", referencedColumnName="id")
     */
    public $partner;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PartnershipGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param PartnershipPartner $partner
     *
     * @return PartnershipGroup
     */
    public function setPartner(PartnershipPartner $partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return PartnershipPartner
     */
    public function getPartner()
    {
        return $this->partner;
    }
}


