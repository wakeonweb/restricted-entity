<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Tests\Doctrine\Walker;

/* imports */
use Doctrine\Tests\OrmTestCase;
use Doctrine\ORM\Query;
use UCS\Component\RestrictedEntity\Doctrine\Walker\RestrictedEntitySqlWalker;
use UCS\Component\RestrictedEntity\Model\OrderBy;
use UCS\Component\RestrictedEntity\Tests\Models\Resolvers\NullValueResolver;
use UCS\Component\RestrictedEntity\Tests\Models\Resolvers\SomeValueResolver;
use UCS\Component\RestrictedEntity\Resolver\ValueResolverRegistry;
use UCS\Component\RestrictedEntity\Strategy\PropertyPathStrategyRegistry;
use UCS\Component\RestrictedEntity\Strategy\FirstNotNullStrategy;

/**
 * RestrictedEntitySqlWalkerTest
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class RestrictedEntitySqlWalkerTest extends OrmTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var ValueResolverRegistry
     */
    private $registry;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->em = $this->_getTestEntityManager();
        $this->registry = new ValueResolverRegistry([
            'null_value' => new NullValueResolver(),
            'some_value' => new SomeValueResolver(),
        ]);

        $this->strategyRegistry = new PropertyPathStrategyRegistry([
            'first_not_null' => new FirstNotNullStrategy,
        ]);
    }

    /**
     * Test on simple DQL
     */
    public function testJoinClientPartnerDQL()
    {
        $this->assertClientPartnerJoinSqlGeneration(
            'select e from UCS\Component\RestrictedEntity\Tests\Models\Partnership\PartnershipEmployee e',
            "SELECT p0_.id AS id_0, p0_.name AS name_1, p0_.client_id AS client_id_2 FROM partnership_employees p0_ INNER JOIN partnership_clients p1_ ON p0_.client_id = p1_.id WHERE p1_.partner_id = ?"
        );
    }

    /**
     * Test on simple DQL
     */
    public function testJoinClientGroupDQL()
    {
        $this->assertClientGroupJoinSqlGeneration(
            'select e from UCS\Component\RestrictedEntity\Tests\Models\Partnership\PartnershipEmployee e',
            "SELECT p0_.id AS id_0, p0_.name AS name_1, p0_.client_id AS client_id_2 FROM partnership_employees p0_ INNER JOIN partnership_clients p1_ ON p0_.client_id = p1_.id WHERE p1_.group_id = ?"
        );
    }

    /**
     * Test on simple DQL
     */
    public function testJoinSecondLevelClientGroupDQL()
    {
        $this->assertClientGroupJoinSecondLevelSqlGeneration(
            'select p from UCS\Component\RestrictedEntity\Tests\Models\Partnership\PartnershipPosition p',
            "SELECT p0_.id AS id_0, p0_.name AS name_1, p0_.employee_id AS employee_id_2 FROM partnership_positions p0_ INNER JOIN partnership_employees p1_ ON p0_.employee_id = p1_.id INNER JOIN partnership_clients p2_ ON p1_.client_id = p2_.id WHERE p2_.group_id = ?"
        );
    }

    /**
     * Assert that the sql generation for the given query are equals
     *
     * @param string $dqlToBeTested
     * @param string $sqlToBeConfirmed
     */
    public function assertClientPartnerJoinSqlGeneration($dqlToBeTested, $sqlToBeConfirmed)
    {
        try {
            $query = $this->em->createQuery($dqlToBeTested);
            $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, array('UCS\Component\RestrictedEntity\Doctrine\Walker\RestrictedEntitySqlWalker'))
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY, $this->registry)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT, new SomeValueResolver)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY, $this->strategyRegistry)
                  ->useQueryCache(false);

            $this->assertEquals($sqlToBeConfirmed, $query->getSql());
            $query->free();
        } catch (\Exception $e) {
            $this->fail($e->getMessage() . ' at "' . $e->getFile() . '" on line ' . $e->getLine());

        }
    }

    /**
     * Assert that the sql generation for the given query are equals
     *
     * @param string $dqlToBeTested
     * @param string $sqlToBeConfirmed
     */
    public function assertClientGroupJoinSqlGeneration($dqlToBeTested, $sqlToBeConfirmed)
    {
        try {
            $query = $this->em->createQuery($dqlToBeTested);
            $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, array('UCS\Component\RestrictedEntity\Doctrine\Walker\RestrictedEntitySqlWalker'))
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY, $this->registry)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT, new NullValueResolver)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY, $this->strategyRegistry)
                  ->useQueryCache(false);

            $this->assertEquals($sqlToBeConfirmed, $query->getSql());
            $query->free();
        } catch (\Exception $e) {
            $this->fail($e->getMessage() . ' at "' . $e->getFile() . '" on line ' . $e->getLine());
        }
    }

    /**
     * Assert that the sql generation for the given query are equals
     *
     * @param string $dqlToBeTested
     * @param string $sqlToBeConfirmed
     */
    public function assertClientGroupJoinSecondLevelSqlGeneration($dqlToBeTested, $sqlToBeConfirmed)
    {
        try {
            $query = $this->em->createQuery($dqlToBeTested);
            $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, array('UCS\Component\RestrictedEntity\Doctrine\Walker\RestrictedEntitySqlWalker'))
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY, $this->registry)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT, new NullValueResolver)
                ->setHint(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY, $this->strategyRegistry)
                  ->useQueryCache(false);

            $this->assertEquals($sqlToBeConfirmed, $query->getSql());
            $query->free();
        } catch (\Exception $e) {
            $this->fail($e->getMessage() . ' at "' . $e->getFile() . '" on line ' . $e->getLine());

        }
    }
}

