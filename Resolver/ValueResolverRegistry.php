<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Resolver;

/**
 * Default registry implementation
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class ValueResolverRegistry implements ValueResolverRegistryInterface
{
    /**
     * @var array
     */
    private $resolvers;

    /**
     * Constructor
     *
     * @param array $resolvers Initial resolvers set
     */
    public function __construct(array $resolvers = array())
    {
        $this->resolvers = $resolvers;
    }

    /**
     * {@inheritdoc}
     */
    public function register(ValueResolverInterface $resolver)
    {
        $name = $resolver->getName();
        unset($this->resolvers[$name]);

        $this->resolvers[$name] = $resolver;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        return isset($this->resolvers[$name]) ? $this->resolvers[$name] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        return isset($this->resolvers[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        unset($this->resolvers[$name]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->resolvers;
    }
}

