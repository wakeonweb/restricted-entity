<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Resolver;

/**
 * Specification for filter resolvers registry
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
interface ValueResolverRegistryInterface
{
    /**
     * Register the given resolver into the interface
     *
     * @param ValueResolver $resolver
     *
     * @return self
     */
    public function register(ValueResolverInterface $resolver);

    /**
     * Get the resolver with the given name
     *
     * @param string $name
     *
     * @return ValueResolverInterface|null
     */
    public function get($name);

    /**
     * Check if the resolver with the given name is registered
     *
     * @param string $name
     *
     * @return boolean weather the resolver exists or not
     */
    public function has($name);

    /**
     * Remove the filtering resolver with the given name
     *
     * @param string $name
     *
     * @return self
     */
    public function remove($name);

    /**
     * Return all filtering resolvers
     *
     * @return array
     */
    public function all();
}

