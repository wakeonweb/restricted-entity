<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Resolver;

/* Imports */
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Resolve the value accordingly to the user property path
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class PropertyAccessorValueResolver implements ValueResolverInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'property_accessor';
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($userPath)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if (!($token = $this->tokenStorage->getToken())) {
            return null;
        }

        if (!($user = $token->getUser())) {
            return null;
        };

        $value = $accessor->getValue($user, $userPath);

        if (is_object($value)) {
            $value = $accessor->getValue($value, 'id');
        }

        return $value;
    }
}


