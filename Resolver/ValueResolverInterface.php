<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Resolver;

/**
 * Specification for user value resolver that can be resolved at run time
 * Resolvers shall be registered in the ValueResolverRegistry and are used
 * to determine at runtime the value that should be taken in the RestrictedEntity
 * userPath query clause.
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
interface ValueResolverInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * Resolve the user value
     *
     * @param string $userPath
     *
     * @return mixed
     */
    public function resolve($userPath);
}

