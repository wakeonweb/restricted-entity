<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace UCS\Component\RestrictedEntity\Annotation;

/* Imports */
use Doctrine\Common\Annotations\Annotation;

/**
 * Annotation used to define restricted datas
 *
 * @Annotation
 * @Target("CLASS")
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
final class RestrictedEntity extends Annotation
{
    /** 
     * @var array<RestrictedEntityPath>
     */
    public $paths;


    /**
     * @var string
     */
    public $strategy;
}

