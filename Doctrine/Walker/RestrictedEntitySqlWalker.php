<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 Nicolas Macherey <nicolas.macherey@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Doctrine\Walker;

/* imports */
use Symfony\Component\PropertyAccess\PropertyPath;
use Doctrine\ORM\Query\TreeWalkerAdapter;
use Doctrine\ORM\Query\AST\PathExpression;
use Doctrine\ORM\Query\AST\SelectStatement;
use Doctrine\ORM\Query\AST\JoinAssociationPathExpression;
use Doctrine\ORM\Query\AST\JoinAssociationDeclaration;
use Doctrine\ORM\Query\AST\Join;
use Doctrine\ORM\Query\AST\RangeVariableDeclaration;
use Doctrine\ORM\Query\AST\ComparisonExpression;
use Doctrine\ORM\Query\AST\OrderByItem;
use Doctrine\ORM\Query\AST\OrderByClause;
use Doctrine\ORM\Query\AST\ConditionalPrimary;
use Doctrine\ORM\Query\AST\ConditionalTerm;
use Doctrine\ORM\Query\AST\ConditionalExpression;
use Doctrine\ORM\Query\AST\InputParameter;
use Doctrine\ORM\Query\AST\ArithmeticExpression;
use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;
use Doctrine\ORM\Query\AST\InExpression;
use Doctrine\ORM\Query\AST\WhereClause;
use Doctrine\ORM\Query;
use Doctrine\Common\Annotations\AnnotationReader;
use Metadata\MetadataFactory;
use UCS\Component\RestrictedEntity\Annotation\RestrictedEntity;
use UCS\Component\RestrictedEntity\Metadata\ClassMetadata;
use UCS\Component\RestrictedEntity\Metadata\Driver\AnnotationDriver;
use UCS\Component\RestrictedEntity\Doctrine\ORM\ContainerAwareEntityManagerInterface;

/**
 * Walker for performing all RestrictedEntity tasks.
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class RestrictedEntitySqlWalker extends TreeWalkerAdapter
{
    /**
     * Registry instance
     *
     * @var string
     */
    const HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY = '_ucs.restricted_entity.hint.value_resolver_registry';

    /**
     * Default resolver
     *
     * @var string
     */
    const HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT = '_ucs.restricted_entity.hint.value_resolver_default';

    /**
     * Registry instance
     *
     * @var string
     */
    const HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY = '_ucs.restricted_entity.hint.strategy_registry';

    /**
     * Registry instance
     *
     * @var string
     */
    const HINT_UCS_RESTRICTED_ENTITY_DISABLED = '_ucs.restricted_entity.hint.disabled';

    /**
     * @var array
     */
    private $paths = array();

    /**
     * @var string
     */
    private $strategy;

    /**
     * @var string
     */
    private $dataClass;

    /**
     * {@inheritdoc}
     */
    public function walkSelectStatement(SelectStatement $selectStatement)
    {
        $q = $this->_getQuery();
        $disabled = $q->getHint(self::HINT_UCS_RESTRICTED_ENTITY_DISABLED);

        if ($disabled === true) {
            return;
        }

        // Get the root entity and alias from the selectStatement fromClause
        $from = $selectStatement->fromClause->identificationVariableDeclarations;

        if (count($from) > 1) {
            return;
        }

        $identificationVariableDeclaration = reset($from);
        $metadata = $this
            ->getFactory()
            ->getMetadataForClass($identificationVariableDeclaration
                ->rangeVariableDeclaration
                ->abstractSchemaName);

        $this->paths = $metadata->getPropertyPaths();
        $this->strategy = $metadata->getStrategy();

        if (count($this->paths) == 0) {
            return;
        }

        $this->dataClass = $identificationVariableDeclaration->rangeVariableDeclaration->abstractSchemaName;
        $this->loadUserValues();
        $this->filterPaths();

        if (count($this->paths) == 0) {
            return;
        }

        $this->addJoinStatements($selectStatement, $identificationVariableDeclaration);
        $this->addWhereStatements($selectStatement, $identificationVariableDeclaration);
    }

    /**
     * Load user values in the application using potential property resolver
     */
    private function loadUserValues()
    {
        $q = $this->_getQuery();
        $registry = $q->getHint(self::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY);
        $defaultResolver = $q->getHint(self::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT);

        foreach ($this->paths as $index => $propertyInfo) {
            $resolver = $defaultResolver;

            if (isset($propertyInfo['resolver'])) {
                $resolver = $registry->get($propertyInfo['resolver']);
            }

            $this->paths[$index]['value'] = $resolver->resolve($propertyInfo['userPath']);
        }
    }

    /**
     * filter path accordingly to the given strategy
     */
    private function filterPaths()
    {
        $q = $this->_getQuery();
        $registry = $q->getHint(self::HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY);

        if ($this->strategy !== null) {
            $strategy = $registry->get($this->strategy);
            $this->paths = $strategy->filter($this->paths);
        }
    }

    /**
     * Creates a meta data factory
     *
     * @return MetadataFactory
     */
    private function getFactory()
    {
        $factory = new MetadataFactory(new AnnotationDriver(new AnnotationReader()));
        $factory->setIncludeInterfaces(true);

        return $factory;
    }

    /**
     * Add join statements in the query.
     *
     * @param SelectStatement $selectStatement
     * @param object          $identificationVariableDecl
     *
     * @return void
     */
    private function addJoinStatements(SelectStatement $selectStatement, $identificationVariableDecl)
    {
        $q = $this->_getQuery();
        $entityManager   = $q->getEntityManager();

        $propertyPaths = $this->paths;
        if (!$propertyPaths || empty($propertyPaths)) {
            return;
        }

        $joins = [];
        foreach ($propertyPaths as $propertyPath) {
            $joins = array_merge($joins, $this->getJoin($propertyPath));
        }

        // No joins return
        if (!$joins || empty($joins)) {
            return;
        }

        $dataClass = $this->dataClass;
        $classMetadata    = $entityManager->getClassMetadata($dataClass);

        if (null === $dataClass) {
            throw new \RuntimeException('You must set the dataClass to use the RestrictedEntitySqlWalker');
        }

        $rangeVariableDecl       = $identificationVariableDecl->rangeVariableDeclaration;

        foreach ($joins as $joinData) {
            $this->addJoinForProperty($identificationVariableDecl, $rangeVariableDecl->aliasIdentificationVariable, $classMetadata, $joinData);
        }
    }

    /**
     * Recursively add join tables.
     *
     * @param mixed         $identificationVariableDecl
     * @param string        $parent
     * @param ClassMetadata $classMetadata
     * @param array         $joinData
     */
    public function addJoinForProperty($identificationVariableDecl, $parent, $classMetadata, $joinData)
    {
        $q = $this->_getQuery();
        $entityManager   = $q->getEntityManager();

        $property   = $joinData['property'];
        $alias      = $joinData['alias'];

        $associationClassName    = $classMetadata->getAssociationTargetClass($property);
        $associationMetadata     = $entityManager->getClassMetadata($associationClassName);
        $mapping                 = $classMetadata->getAssociationMapping($property);
        $joinType = Join::JOIN_TYPE_INNER;

        $joinAssocPathExpression = new JoinAssociationPathExpression($parent, $property);
        $joinAssocDeclaration    = new JoinAssociationDeclaration($joinAssocPathExpression, $alias, null);
        $join = new Join($joinType, $joinAssocDeclaration);

        // We now need to check if the join is already made or not
        $found = false;
        foreach ($identificationVariableDecl->joins as $existingJoin) {
            if ($existingJoin->joinAssociationDeclaration instanceof JoinAssociationDeclaration
                && $joinType == $existingJoin->joinType
                && $alias == $existingJoin->joinAssociationDeclaration->aliasIdentificationVariable
                && $parent == $existingJoin->joinAssociationDeclaration->joinAssociationPathExpression->identificationVariable
                && $property == $existingJoin->joinAssociationDeclaration->joinAssociationPathExpression->associationField) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            $identificationVariableDecl->joins[] = $join;
            $this->setQueryComponent($alias,
                array(
                    'metadata'     => $associationMetadata,
                    'parent'       => $parent,
                    'relation'     => $mapping,
                    'map'          => null,
                    'nestingLevel' => 0,
                    'token'        => null,
                )
            );
        }

        foreach ($joinData['joins'] as $subJoinData) {
            $this->addJoinForProperty($identificationVariableDecl, $alias, $associationMetadata, $subJoinData);
        }
    }

    /**
     * Get the joins from the given property path
     *
     * @param array $pathInfo
     *
     * @return array
     */
    public function getJoin($pathInfo)
    {
        if ($pathInfo['value'] === null) {
            return [];
        }

        $propertyPath = new PropertyPath($pathInfo['propertyPath']);
        $elements = $propertyPath->getElements();
        $joins = array();

        $current = &$joins;
        // We have a join if the element count is greater than 1
        if (count($elements) > 1) {
            array_pop($elements);
            foreach ($elements as $joinProperty) {
                $toAdd = array(
                    'property' => $joinProperty,
                    'alias' => $joinProperty,
                    'joins' => array(),
                );

                $current[] = $toAdd;
                $index = count($current)-1;
                $current =& $current[$index]['joins'];
            }
        }

        return $joins;
    }

    /**
     * Add where statements in the query.
     *
     * @param SelectStatement $selectStatement
     * @param [type]          $identificationVariableDecl
     */
    private function addWhereStatements(SelectStatement $selectStatement, $identificationVariableDecl)
    {
        $propertyPaths = $this->paths;

        // No whereExprs return
        if (!$propertyPaths || empty($propertyPaths)) {
            return;
        }

        // Walk all where expressions so that we can create the required additionnal statements
        $rangeVariableDecl       = $identificationVariableDecl->rangeVariableDeclaration;
        $root = $rangeVariableDecl->aliasIdentificationVariable;

        foreach ($propertyPaths as $pathInfo) {
            $q = $this->_getQuery();

            $pathExpression = $this->getPathExpression($root, $pathInfo);
            $expression = new ComparisonExpression($pathExpression, '=', $pathInfo['value']);

            $conditionalPrimary = new ConditionalPrimary();
            $conditionalPrimary->simpleConditionalExpression = $expression;

            if ($selectStatement->whereClause) {
                if ($selectStatement->whereClause->conditionalExpression instanceof ConditionalTerm) {
                    $selectStatement->whereClause->conditionalExpression->conditionalFactors[] = $conditionalPrimary;
                } elseif ($selectStatement->whereClause->conditionalExpression instanceof ConditionalPrimary) {
                    $selectStatement->whereClause->conditionalExpression = new ConditionalExpression(array(
                        new ConditionalTerm(array(
                            $selectStatement->whereClause->conditionalExpression,
                            $conditionalPrimary,
                        )),
                    ));
                } elseif ($selectStatement->whereClause->conditionalExpression instanceof ConditionalExpression) {
                    $tmpPrimary = new ConditionalPrimary();
                    $tmpPrimary->conditionalExpression = $selectStatement->whereClause->conditionalExpression;
                    $selectStatement->whereClause->conditionalExpression = new ConditionalTerm(array(
                        $tmpPrimary,
                        $conditionalPrimary,
                    ));
                }
            } else {
                // If not existant simply add the clause
                $selectStatement->whereClause = new WhereClause(
                    new ConditionalTerm(array(
                        $conditionalPrimary,
                    ))
                );
            }
        }
    }

    /**
     * Build a path expression from the given alias and filter
     *
     * @param string $alias
     * @param array  $pathInfo
     *
     * @return PathExpression
     */
    public function getPathExpression($alias, array $pathInfo)
    {
        $propertyPath = new PropertyPath($pathInfo['propertyPath']);
        $elements = $propertyPath->getElements();
        $count = count($elements);

        if ($count == 0) {
            return;
        }

        $type = PathExpression::TYPE_SINGLE_VALUED_ASSOCIATION;

        if ($count == 1) {
            // This is the proper path expression to build the query
            $pathExpression = new PathExpression($type, $alias, $elements[0]);
            $pathExpression->type = $type;
        } else {
            $alias = $elements[$count-2];
            $property = $elements[$count-1];

            $pathExpression = new PathExpression($type, $alias, $property);
            $pathExpression->type = $type;
        }

        return $pathExpression;
    }
}