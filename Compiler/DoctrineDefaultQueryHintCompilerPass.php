<?php

namespace UCS\Component\RestrictedEntity\Compiler;

use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use UCS\Component\RestrictedEntity\Doctrine\Walker\RestrictedEntitySqlWalker;

/**
* Compiler pass to register the doctrine logger
*
* @author Nicolas Macherey <nicolas.macherey@gmail.com>
*/
class DoctrineDefaultQueryHintCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('doctrine.orm.default_configuration')) {
            return;
        }

        $definition = $container->getDefinition('doctrine.orm.default_configuration');
        $resolverRegistry = new Reference('ucs.value_resolver_registry');
        $defaultResolver = new Reference('ucs.default_value_resolver');
        $strategyRegistry = new Reference('ucs.property_path_strategy_registry');

        $definition->addMethodCall(
            'setDefaultQueryHint',
            array(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_REGISTRY, $resolverRegistry)
        );

        $definition->addMethodCall(
            'setDefaultQueryHint',
            array(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_VALUE_RESOLVER_DEFAULT, $defaultResolver)
        );

        $definition->addMethodCall(
            'setDefaultQueryHint',
            array(RestrictedEntitySqlWalker::HINT_UCS_RESTRICTED_ENTITY_STRATEGY_REGISTRY, $strategyRegistry)
        );

        $definition->addMethodCall(
            'setDefaultQueryHint',
            array(Query::HINT_CUSTOM_TREE_WALKERS, array('UCS\\Component\\RestrictedEntity\\Doctrine\\Walker\\RestrictedEntitySqlWalker'))
        );
    }
}

