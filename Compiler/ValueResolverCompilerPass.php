<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 Nicolas Macherey <nicolas.macherey@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
* Collects annotation for filters
*
* @author Nicolas Macherey <nicolas.macherey@gmail.com>
*/
class ValueResolverCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('ucs.value_resolver_registry')) {
            return;
        }

        $definition = $container->getDefinition('ucs.value_resolver_registry');
        // Builds an array with service IDs as keys and tag aliases as values
        $strategies = array();

        foreach ($container->findTaggedServiceIds('ucs.value_resolver') as $serviceId => $tag) {
            $alias = isset($tag[0]['alias'])
                ? $tag[0]['alias']
                : $serviceId;

            $strategies[$alias] = new Reference($serviceId);
        }

        $definition->replaceArgument(0, $strategies);
    }
}
