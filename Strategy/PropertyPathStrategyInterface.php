<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Strategy;

/**
 * Specification for user value resolver that can be resolved at run time
 * Resolvers shall be registered in the ValueResolverRegistry and are used
 * to determine at runtime the value that should be taken in the RestrictedEntity
 * userPath query clause.
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
interface PropertyPathStrategyInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * Filter the original property path array accordingly to the 
     * resolved values. Keep only available paths that will be used in the query
     *
     * @param array $propertyPaths
     *
     * @return array
     */
    public function filter($propertyPaths);
}


