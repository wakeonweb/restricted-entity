<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Strategy;

/**
 * Default registry implementation
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class PropertyPathStrategyRegistry implements PropertyPathStrategyRegistryInterface
{
    /**
     * @var array
     */
    private $strategys;

    /**
     * Constructor
     *
     * @param array $strategys Initial strategys set
     */
    public function __construct(array $strategys = array())
    {
        $this->strategys = $strategys;
    }

    /**
     * {@inheritdoc}
     */
    public function register(PropertyPathStrategyInterface $strategy)
    {
        $name = $strategy->getName();
        unset($this->strategys[$name]);

        $this->strategys[$name] = $strategy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        return isset($this->strategys[$name]) ? $this->strategys[$name] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        return isset($this->strategys[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        unset($this->strategys[$name]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->strategys;
    }
}


