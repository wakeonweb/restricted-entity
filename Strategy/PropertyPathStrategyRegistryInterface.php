<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 UCS <http://www.ucs-labs.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UCS\Component\RestrictedEntity\Strategy;

/**
 * Specification for filter strategys registry
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
interface PropertyPathStrategyRegistryInterface
{
    /**
     * Register the given strategy into the interface
     *
     * @param PropertyPathStrategy $strategy
     *
     * @return self
     */
    public function register(PropertyPathStrategyInterface $strategy);

    /**
     * Get the strategy with the given name
     *
     * @param string $name
     *
     * @return PropertyPathStrategyInterface|null
     */
    public function get($name);

    /**
     * Check if the strategy with the given name is registered
     *
     * @param string $name
     *
     * @return boolean weather the strategy exists or not
     */
    public function has($name);

    /**
     * Remove the filtering strategy with the given name
     *
     * @param string $name
     *
     * @return self
     */
    public function remove($name);

    /**
     * Return all filtering strategys
     *
     * @return array
     */
    public function all();
}


