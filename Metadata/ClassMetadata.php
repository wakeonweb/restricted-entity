<?php

/*
 * This file is part of the UCS package.
 *
 * Copyright 2014 Nicolas Macherey <nicolas.macherey@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace UCS\Component\RestrictedEntity\Metadata;

/* Imports */
use Metadata\MergeableInterface;
use Metadata\MergeableClassMetadata;

/* Local Imports */
use UCS\Component\RestrictedEntity\Exception\InvalidArgumentException;

/**
 * Contains class metadata information
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class ClassMetadata extends MergeableClassMetadata
{
    /**
     * @var array
     */
    protected $propertyPaths = array();

    /**
     * @var string
     */
    protected $strategy;

    /**
     * Merge with the given metadata
     *
     * @param MergeableInterface $metadata
     */
    public function merge(MergeableInterface $metadata)
    {
        if (!$metadata instanceof ClassMetadata) {
            throw new InvalidArgumentException('$metadata must be an instance of ClassMetadata.');
        }

        $this->propertyPaths = array_merge($this->propertyPaths, $metadata->getPropertyPaths());
        parent::merge($metadata);
    }

    /**
     * Gets the value of paths.
     *
     * @return array
     */
    public function getPropertyPaths()
    {
        return $this->propertyPaths;
    }

    /**
     * Sets the value of propertyPaths.
     *
     * @param array $propertyPaths
     *
     * @return self
     */
    public function setPropertyPaths(array $propertyPaths)
    {
        $this->propertyPaths = (array) $propertyPaths;

        return $this;
    }

    /**
     * @return string
     */
    public function getStrategy()
    {
        return $this->strategy;
    }

    /**
     * @param string $strategy
     *
     * @return self
     */
    public function setStrategy($strategy)
    {
        $this->strategy = $strategy;

        return $this;
    }
}
